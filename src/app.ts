import Koa from 'koa'
import cors from '@koa/cors'
import bodyParser from 'koa-bodyparser'
import HttpStatusCode from "./common/constant/http-code.constants";
import { ApiException } from "./common/exception/api.exception";
import logger from "./common/log/winston";
import allRouters from './routes/index'
const app = new Koa()
import "./models/mysql/init-models"
import "./models/redis/Redis"


/**
 * 统一异常处理
 */
app.use(async (ctx: Koa.Context, next: Koa.Next) => {
    try {
        await next()
    } catch (e) {
        let code = HttpStatusCode.INTERNAL_SERVER_ERROR
        let message = 'Internal Server Error'

        if (e instanceof ApiException) {
            code = e.errorCode ?? code
            message = e.errorMsg ?? message
        } else {
            logger.error('[Error]', e)
        }
        ctx.status = code
        ctx.body = {
            status: code,
            message,
        }
    }
})

/**
 * 跨域中间件
 */
app.use(cors({
    origin: ({ request }) => request?.header?.origin ?? '*',
    allowMethods: 'GET,HEAD,PUT,POST,DELETE,PATCH,OPTIONS',
    allowHeaders: 'content-type,test-id,site-id,x-version,x-app-id',
    credentials: true
}))

/**
 * 用于解析客户端请求的body中的内容,内部使用JSON编码处理,url编码处理以及对于文件的上传处理
 */
app.use(bodyParser())


/**
 * 路由注册中间件
 */
app.use(allRouters.routes())
app.use(allRouters.allowedMethods())



export default app
