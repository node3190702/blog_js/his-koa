import * as dotenv from 'dotenv'
dotenv.config({
    path: process.env.NODE_ENV === 'dev'? '.env.development': '.env.production'
})
import app from './app'
import http from 'http'
import logger from "./common/log/winston";
const server = http.createServer(app.callback())

/**
 * 设置server的端口以及host
 */
const { PORT, HOST, SERVER_NAME } = getServerInfo()
server.listen(PORT)

/**
 *  监听server的启动
 */
server.on('listening', () => {
    logger.info(`${SERVER_NAME} is running at http://${HOST}:${PORT}`)
})


/**
 * 获取server端的端口,host以及名称
 */
function getServerInfo(): any {
    const PORT: string = process.env.SERVER_PORT || '3000'
    const HOST: string = process.env.SERVER_HOST || '127.0.0.1'
    const SERVER_NAME = process.env.SERVER_NAME || 'server'
    return {
        PORT: Number(PORT),
        HOST,
        SERVER_NAME
    }
}
