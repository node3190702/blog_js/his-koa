import Redis from 'ioredis'
import { RedisConf } from '../../common/configuration/redis.configuration'
import logger from "../../common/log/winston";

const redisUtils = new Redis({
    host: RedisConf.REDIS.HOST,
    port: RedisConf.REDIS.PORT,
    username: RedisConf.REDIS.USERNAME,
    password: RedisConf.REDIS.PASSWORD
})

// 连接成功后触发
redisUtils.on('ready', () => {
    let logs = `Redis connected at port ${process.env.REDIS_PORT}`
    logger.info(logs)
})

// 连接出错时触发
redisUtils.on('error', (err) => {
    let errInfo  =`Redis error: ${err}`
    logger.error(errInfo)
})

// 关闭连接时触发
redisUtils.on('end', () => {
    logger.warn('Redis disconnected')
})

export default redisUtils
