import * as Sequelize from 'sequelize';
import { DataTypes, Model, Optional } from 'sequelize';

export interface tb_permissionAttributes {
  id: number;
  permission_name: string;
  module_id: number;
  action_id: number;
}

export type tb_permissionPk = "id";
export type tb_permissionId = tb_permission[tb_permissionPk];
export type tb_permissionCreationAttributes = tb_permissionAttributes;

export class tb_permission extends Model<tb_permissionAttributes, tb_permissionCreationAttributes> implements tb_permissionAttributes {
  id!: number;
  permission_name!: string;
  module_id!: number;
  action_id!: number;


  static initModel(sequelize: Sequelize.Sequelize): typeof tb_permission {
    return tb_permission.init({
    id: {
      type: DataTypes.INTEGER.UNSIGNED,
      allowNull: false,
      primaryKey: true,
      comment: "主键"
    },
    permission_name: {
      type: DataTypes.STRING(200),
      allowNull: false,
      comment: "权限",
      unique: "unq_permission"
    },
    module_id: {
      type: DataTypes.INTEGER.UNSIGNED,
      allowNull: false,
      comment: "模块ID"
    },
    action_id: {
      type: DataTypes.INTEGER.UNSIGNED,
      allowNull: false,
      comment: "行为ID"
    }
  }, {
    sequelize,
    tableName: 'tb_permission',
    timestamps: false,
    indexes: [
      {
        name: "PRIMARY",
        unique: true,
        using: "BTREE",
        fields: [
          { name: "id" },
        ]
      },
      {
        name: "unq_permission",
        unique: true,
        using: "BTREE",
        fields: [
          { name: "permission_name" },
        ]
      },
    ]
  });
  }
}
