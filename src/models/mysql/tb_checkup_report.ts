import * as Sequelize from 'sequelize';
import { DataTypes, Model, Optional } from 'sequelize';

export interface tb_checkup_reportAttributes {
  id: number;
  appointment_id: number;
  result_id: string;
  status: number;
  file_path?: string;
  waybill_code?: string;
  waybill_date?: string;
  date: string;
  create_time: Date;
}

export type tb_checkup_reportPk = "id";
export type tb_checkup_reportId = tb_checkup_report[tb_checkup_reportPk];
export type tb_checkup_reportOptionalAttributes = "id" | "file_path" | "waybill_code" | "waybill_date";
export type tb_checkup_reportCreationAttributes = Optional<tb_checkup_reportAttributes, tb_checkup_reportOptionalAttributes>;

export class tb_checkup_report extends Model<tb_checkup_reportAttributes, tb_checkup_reportCreationAttributes> implements tb_checkup_reportAttributes {
  id!: number;
  appointment_id!: number;
  result_id!: string;
  status!: number;
  file_path?: string;
  waybill_code?: string;
  waybill_date?: string;
  date!: string;
  create_time!: Date;


  static initModel(sequelize: Sequelize.Sequelize): typeof tb_checkup_report {
    return tb_checkup_report.init({
    id: {
      autoIncrement: true,
      type: DataTypes.INTEGER,
      allowNull: false,
      primaryKey: true,
      comment: "主键"
    },
    appointment_id: {
      type: DataTypes.INTEGER,
      allowNull: false,
      comment: "体检预约ID",
      unique: "unq_appointment_id"
    },
    result_id: {
      type: DataTypes.STRING(24),
      allowNull: false,
      comment: "体检结果ID（MongoDB）",
      unique: "unq_result_id"
    },
    status: {
      type: DataTypes.TINYINT,
      allowNull: false,
      comment: "体检报告状态。1未生成，2已生成，3已邮寄"
    },
    file_path: {
      type: DataTypes.STRING(300),
      allowNull: true,
      comment: "提交报告存放在Minio服务器上的URL地址"
    },
    waybill_code: {
      type: DataTypes.STRING(200),
      allowNull: true,
      comment: "快递运单号"
    },
    waybill_date: {
      type: DataTypes.DATEONLY,
      allowNull: true,
      comment: "快递发出日期"
    },
    date: {
      type: DataTypes.DATEONLY,
      allowNull: false,
      comment: "体检日期"
    },
    create_time: {
      type: DataTypes.DATE,
      allowNull: false,
      comment: "创建时间"
    }
  }, {
    sequelize,
    tableName: 'tb_checkup_report',
    timestamps: false,
    indexes: [
      {
        name: "PRIMARY",
        unique: true,
        using: "BTREE",
        fields: [
          { name: "id" },
        ]
      },
      {
        name: "unq_appointment_id",
        unique: true,
        using: "BTREE",
        fields: [
          { name: "appointment_id" },
        ]
      },
      {
        name: "unq_result_id",
        unique: true,
        using: "BTREE",
        fields: [
          { name: "result_id" },
        ]
      },
      {
        name: "idx_status",
        using: "BTREE",
        fields: [
          { name: "status" },
        ]
      },
      {
        name: "idx_waybill_code",
        using: "BTREE",
        fields: [
          { name: "waybill_code" },
        ]
      },
    ]
  });
  }
}
