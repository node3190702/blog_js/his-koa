import * as Sequelize from 'sequelize';
import { DataTypes, Model, Optional } from 'sequelize';

export interface tb_customerAttributes {
  id: number;
  name?: string;
  sex?: string;
  tel: string;
  photo?: string;
  create_time: Date;
}

export type tb_customerPk = "id";
export type tb_customerId = tb_customer[tb_customerPk];
export type tb_customerOptionalAttributes = "id" | "name" | "sex" | "photo" | "create_time";
export type tb_customerCreationAttributes = Optional<tb_customerAttributes, tb_customerOptionalAttributes>;

export class tb_customer extends Model<tb_customerAttributes, tb_customerCreationAttributes> implements tb_customerAttributes {
  id!: number;
  name?: string;
  sex?: string;
  tel!: string;
  photo?: string;
  create_time!: Date;


  static initModel(sequelize: Sequelize.Sequelize): typeof tb_customer {
    return tb_customer.init({
    id: {
      autoIncrement: true,
      type: DataTypes.INTEGER,
      allowNull: false,
      primaryKey: true,
      comment: "主键"
    },
    name: {
      type: DataTypes.STRING(200),
      allowNull: true,
      comment: "姓名"
    },
    sex: {
      type: DataTypes.CHAR(1),
      allowNull: true,
      comment: "性别"
    },
    tel: {
      type: DataTypes.CHAR(11),
      allowNull: false,
      comment: "电话",
      unique: "idx_tel"
    },
    photo: {
      type: DataTypes.STRING(200),
      allowNull: true,
      comment: "照片URL"
    },
    create_time: {
      type: DataTypes.DATE,
      allowNull: false,
      defaultValue: Sequelize.Sequelize.literal('CURRENT_TIMESTAMP'),
      comment: "注册时间"
    }
  }, {
    sequelize,
    tableName: 'tb_customer',
    timestamps: false,
    indexes: [
      {
        name: "PRIMARY",
        unique: true,
        using: "BTREE",
        fields: [
          { name: "id" },
        ]
      },
      {
        name: "idx_tel",
        unique: true,
        using: "BTREE",
        fields: [
          { name: "tel" },
        ]
      },
    ]
  });
  }
}
