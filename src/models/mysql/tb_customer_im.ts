import * as Sequelize from 'sequelize';
import { DataTypes, Model, Optional } from 'sequelize';

export interface tb_customer_imAttributes {
  id: number;
  customer_id: number;
  login_time: Date;
}

export type tb_customer_imPk = "id";
export type tb_customer_imId = tb_customer_im[tb_customer_imPk];
export type tb_customer_imOptionalAttributes = "id";
export type tb_customer_imCreationAttributes = Optional<tb_customer_imAttributes, tb_customer_imOptionalAttributes>;

export class tb_customer_im extends Model<tb_customer_imAttributes, tb_customer_imCreationAttributes> implements tb_customer_imAttributes {
  id!: number;
  customer_id!: number;
  login_time!: Date;


  static initModel(sequelize: Sequelize.Sequelize): typeof tb_customer_im {
    return tb_customer_im.init({
    id: {
      autoIncrement: true,
      type: DataTypes.INTEGER,
      allowNull: false,
      primaryKey: true,
      comment: "主键ID"
    },
    customer_id: {
      type: DataTypes.INTEGER,
      allowNull: false,
      comment: "客户ID",
      unique: "unq_customer_id"
    },
    login_time: {
      type: DataTypes.DATE,
      allowNull: false,
      comment: "创建时间"
    }
  }, {
    sequelize,
    tableName: 'tb_customer_im',
    timestamps: false,
    indexes: [
      {
        name: "PRIMARY",
        unique: true,
        using: "BTREE",
        fields: [
          { name: "id" },
        ]
      },
      {
        name: "unq_customer_id",
        unique: true,
        using: "BTREE",
        fields: [
          { name: "customer_id" },
        ]
      },
    ]
  });
  }
}
