import * as Sequelize from 'sequelize';
import { DataTypes, Model, Optional } from 'sequelize';

export interface tb_appointment_restrictionAttributes {
  id: number;
  date: string;
  num_1: number;
  num_2: number;
  num_3: number;
  remark?: string;
  create_time: Date;
}

export type tb_appointment_restrictionPk = "id";
export type tb_appointment_restrictionId = tb_appointment_restriction[tb_appointment_restrictionPk];
export type tb_appointment_restrictionOptionalAttributes = "id" | "remark" | "create_time";
export type tb_appointment_restrictionCreationAttributes = Optional<tb_appointment_restrictionAttributes, tb_appointment_restrictionOptionalAttributes>;

export class tb_appointment_restriction extends Model<tb_appointment_restrictionAttributes, tb_appointment_restrictionCreationAttributes> implements tb_appointment_restrictionAttributes {
  id!: number;
  date!: string;
  num_1!: number;
  num_2!: number;
  num_3!: number;
  remark?: string;
  create_time!: Date;


  static initModel(sequelize: Sequelize.Sequelize): typeof tb_appointment_restriction {
    return tb_appointment_restriction.init({
    id: {
      autoIncrement: true,
      type: DataTypes.INTEGER,
      allowNull: false,
      primaryKey: true,
      comment: "主键"
    },
    date: {
      type: DataTypes.DATEONLY,
      allowNull: false,
      comment: "日期",
      unique: "unq_date"
    },
    num_1: {
      type: DataTypes.INTEGER,
      allowNull: false,
      comment: "实际限定体检人数"
    },
    num_2: {
      type: DataTypes.INTEGER,
      allowNull: false,
      comment: "每天体检人数上限"
    },
    num_3: {
      type: DataTypes.INTEGER,
      allowNull: false,
      comment: "实际体检人数"
    },
    remark: {
      type: DataTypes.STRING(200),
      allowNull: true,
      comment: "备注信息"
    },
    create_time: {
      type: DataTypes.DATE,
      allowNull: false,
      defaultValue: Sequelize.Sequelize.literal('CURRENT_TIMESTAMP'),
      comment: "创建时间"
    }
  }, {
    sequelize,
    tableName: 'tb_appointment_restriction',
    timestamps: false,
    indexes: [
      {
        name: "PRIMARY",
        unique: true,
        using: "BTREE",
        fields: [
          { name: "id" },
        ]
      },
      {
        name: "unq_date",
        unique: true,
        using: "BTREE",
        fields: [
          { name: "date" },
        ]
      },
    ]
  });
  }
}
