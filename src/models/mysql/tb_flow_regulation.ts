import * as Sequelize from 'sequelize';
import { DataTypes, Model, Optional } from 'sequelize';

export interface tb_flow_regulationAttributes {
  id: number;
  place: string;
  real_num: number;
  max_num: number;
  weight: number;
  priority: number;
  blue_uuid: string;
}

export type tb_flow_regulationPk = "id";
export type tb_flow_regulationId = tb_flow_regulation[tb_flow_regulationPk];
export type tb_flow_regulationOptionalAttributes = "id" | "real_num" | "weight" | "priority";
export type tb_flow_regulationCreationAttributes = Optional<tb_flow_regulationAttributes, tb_flow_regulationOptionalAttributes>;

export class tb_flow_regulation extends Model<tb_flow_regulationAttributes, tb_flow_regulationCreationAttributes> implements tb_flow_regulationAttributes {
  id!: number;
  place!: string;
  real_num!: number;
  max_num!: number;
  weight!: number;
  priority!: number;
  blue_uuid!: string;


  static initModel(sequelize: Sequelize.Sequelize): typeof tb_flow_regulation {
    return tb_flow_regulation.init({
    id: {
      autoIncrement: true,
      type: DataTypes.INTEGER,
      allowNull: false,
      primaryKey: true,
      comment: "主键"
    },
    place: {
      type: DataTypes.STRING(50),
      allowNull: false,
      comment: "科室名称",
      unique: "unq_place"
    },
    real_num: {
      type: DataTypes.INTEGER,
      allowNull: false,
      defaultValue: 0,
      comment: "排队人数"
    },
    max_num: {
      type: DataTypes.INTEGER,
      allowNull: false,
      comment: "最大人数"
    },
    weight: {
      type: DataTypes.TINYINT,
      allowNull: false,
      defaultValue: 1,
      comment: "权重（自动调流使用）"
    },
    priority: {
      type: DataTypes.TINYINT,
      allowNull: false,
      defaultValue: 1,
      comment: "优先级（手动调流使用）"
    },
    blue_uuid: {
      type: DataTypes.STRING(64),
      allowNull: false,
      comment: "蓝牙信标ID",
      unique: "unq_blue_uuid"
    }
  }, {
    sequelize,
    tableName: 'tb_flow_regulation',
    timestamps: false,
    indexes: [
      {
        name: "PRIMARY",
        unique: true,
        using: "BTREE",
        fields: [
          { name: "id" },
        ]
      },
      {
        name: "unq_place",
        unique: true,
        using: "BTREE",
        fields: [
          { name: "place" },
        ]
      },
      {
        name: "unq_blue_uuid",
        unique: true,
        using: "BTREE",
        fields: [
          { name: "blue_uuid" },
        ]
      },
    ]
  });
  }
}
