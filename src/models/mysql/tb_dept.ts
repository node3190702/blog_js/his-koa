import * as Sequelize from 'sequelize';
import { DataTypes, Model, Optional } from 'sequelize';

export interface tb_deptAttributes {
  id: number;
  dept_name: string;
  tel?: string;
  email?: string;
  desc?: string;
}

export type tb_deptPk = "id";
export type tb_deptId = tb_dept[tb_deptPk];
export type tb_deptOptionalAttributes = "id" | "tel" | "email" | "desc";
export type tb_deptCreationAttributes = Optional<tb_deptAttributes, tb_deptOptionalAttributes>;

export class tb_dept extends Model<tb_deptAttributes, tb_deptCreationAttributes> implements tb_deptAttributes {
  id!: number;
  dept_name!: string;
  tel?: string;
  email?: string;
  desc?: string;


  static initModel(sequelize: Sequelize.Sequelize): typeof tb_dept {
    return tb_dept.init({
    id: {
      autoIncrement: true,
      type: DataTypes.INTEGER.UNSIGNED,
      allowNull: false,
      primaryKey: true,
      comment: "主键"
    },
    dept_name: {
      type: DataTypes.STRING(200),
      allowNull: false,
      comment: "部门名称",
      unique: "unq_dept_name"
    },
    tel: {
      type: DataTypes.STRING(20),
      allowNull: true,
      comment: "部门电话"
    },
    email: {
      type: DataTypes.STRING(200),
      allowNull: true,
      comment: "部门邮箱"
    },
    desc: {
      type: DataTypes.STRING(20),
      allowNull: true,
      comment: "备注"
    }
  }, {
    sequelize,
    tableName: 'tb_dept',
    timestamps: false,
    indexes: [
      {
        name: "PRIMARY",
        unique: true,
        using: "BTREE",
        fields: [
          { name: "id" },
        ]
      },
      {
        name: "unq_dept_name",
        unique: true,
        using: "BTREE",
        fields: [
          { name: "dept_name" },
        ]
      },
    ]
  });
  }
}
