import * as Sequelize from 'sequelize';
import { DataTypes, Model, Optional } from 'sequelize';

export interface tb_moduleAttributes {
  id: number;
  module_code: string;
  module_name: string;
}

export type tb_modulePk = "id";
export type tb_moduleId = tb_module[tb_modulePk];
export type tb_moduleCreationAttributes = tb_moduleAttributes;

export class tb_module extends Model<tb_moduleAttributes, tb_moduleCreationAttributes> implements tb_moduleAttributes {
  id!: number;
  module_code!: string;
  module_name!: string;


  static initModel(sequelize: Sequelize.Sequelize): typeof tb_module {
    return tb_module.init({
    id: {
      type: DataTypes.INTEGER.UNSIGNED,
      allowNull: false,
      primaryKey: true,
      comment: "主键"
    },
    module_code: {
      type: DataTypes.STRING(200),
      allowNull: false,
      comment: "模块编号",
      unique: "unq_module_id"
    },
    module_name: {
      type: DataTypes.STRING(200),
      allowNull: false,
      comment: "模块名称"
    }
  }, {
    sequelize,
    tableName: 'tb_module',
    timestamps: false,
    indexes: [
      {
        name: "PRIMARY",
        unique: true,
        using: "BTREE",
        fields: [
          { name: "id" },
        ]
      },
      {
        name: "unq_module_id",
        unique: true,
        using: "BTREE",
        fields: [
          { name: "module_code" },
        ]
      },
    ]
  });
  }
}
