import * as Sequelize from 'sequelize';
import { DataTypes, Model, Optional } from 'sequelize';

export interface tb_ruleAttributes {
  id: number;
  name: string;
  rule: string;
  remark?: string;
}

export type tb_rulePk = "id";
export type tb_ruleId = tb_rule[tb_rulePk];
export type tb_ruleOptionalAttributes = "id" | "remark";
export type tb_ruleCreationAttributes = Optional<tb_ruleAttributes, tb_ruleOptionalAttributes>;

export class tb_rule extends Model<tb_ruleAttributes, tb_ruleCreationAttributes> implements tb_ruleAttributes {
  id!: number;
  name!: string;
  rule!: string;
  remark?: string;


  static initModel(sequelize: Sequelize.Sequelize): typeof tb_rule {
    return tb_rule.init({
    id: {
      autoIncrement: true,
      type: DataTypes.INTEGER,
      allowNull: false,
      primaryKey: true,
      comment: "主键"
    },
    name: {
      type: DataTypes.STRING(200),
      allowNull: false,
      comment: "规则名称",
      unique: "unq_name"
    },
    rule: {
      type: DataTypes.TEXT,
      allowNull: false,
      comment: "价格计算规则"
    },
    remark: {
      type: DataTypes.STRING(200),
      allowNull: true,
      comment: "备注"
    }
  }, {
    sequelize,
    tableName: 'tb_rule',
    timestamps: false,
    indexes: [
      {
        name: "PRIMARY",
        unique: true,
        using: "BTREE",
        fields: [
          { name: "id" },
        ]
      },
      {
        name: "unq_name",
        unique: true,
        using: "BTREE",
        fields: [
          { name: "name" },
        ]
      },
    ]
  });
  }
}
