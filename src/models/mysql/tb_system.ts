import * as Sequelize from 'sequelize';
import { DataTypes, Model, Optional } from 'sequelize';

export interface tb_systemAttributes {
  id: number;
  item: string;
  value: string;
  remark?: string;
}

export type tb_systemPk = "id";
export type tb_systemId = tb_system[tb_systemPk];
export type tb_systemOptionalAttributes = "id" | "remark";
export type tb_systemCreationAttributes = Optional<tb_systemAttributes, tb_systemOptionalAttributes>;

export class tb_system extends Model<tb_systemAttributes, tb_systemCreationAttributes> implements tb_systemAttributes {
  id!: number;
  item!: string;
  value!: string;
  remark?: string;


  static initModel(sequelize: Sequelize.Sequelize): typeof tb_system {
    return tb_system.init({
    id: {
      autoIncrement: true,
      type: DataTypes.INTEGER,
      allowNull: false,
      primaryKey: true,
      comment: "主键"
    },
    item: {
      type: DataTypes.STRING(200),
      allowNull: false,
      comment: "设置项"
    },
    value: {
      type: DataTypes.STRING(200),
      allowNull: false,
      comment: "设定值"
    },
    remark: {
      type: DataTypes.STRING(255),
      allowNull: true,
      comment: "备注"
    }
  }, {
    sequelize,
    tableName: 'tb_system',
    timestamps: false,
    indexes: [
      {
        name: "PRIMARY",
        unique: true,
        using: "BTREE",
        fields: [
          { name: "id" },
        ]
      },
    ]
  });
  }
}
