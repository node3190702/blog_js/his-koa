import * as Sequelize from 'sequelize';
import { DataTypes, Model, Optional } from 'sequelize';

export interface tb_actionAttributes {
  id: number;
  action_code: string;
  action_name: string;
}

export type tb_actionPk = "id";
export type tb_actionId = tb_action[tb_actionPk];
export type tb_actionCreationAttributes = tb_actionAttributes;

export class tb_action extends Model<tb_actionAttributes, tb_actionCreationAttributes> implements tb_actionAttributes {
  id!: number;
  action_code!: string;
  action_name!: string;


  static initModel(sequelize: Sequelize.Sequelize): typeof tb_action {
    return tb_action.init({
    id: {
      type: DataTypes.INTEGER.UNSIGNED,
      allowNull: false,
      primaryKey: true,
      comment: "主键"
    },
    action_code: {
      type: DataTypes.STRING(200),
      allowNull: false,
      comment: "行为编号"
    },
    action_name: {
      type: DataTypes.STRING(200),
      allowNull: false,
      comment: "行为名称",
      unique: "unq_action_name"
    }
  }, {
    sequelize,
    tableName: 'tb_action',
    timestamps: false,
    indexes: [
      {
        name: "PRIMARY",
        unique: true,
        using: "BTREE",
        fields: [
          { name: "id" },
        ]
      },
      {
        name: "unq_action_name",
        unique: true,
        using: "BTREE",
        fields: [
          { name: "action_name" },
        ]
      },
    ]
  });
  }
}
