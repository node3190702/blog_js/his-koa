import * as Sequelize from 'sequelize';
import { DataTypes, Model, Optional } from 'sequelize';

export interface tb_goodsAttributes {
  id: number;
  code: string;
  title: string;
  description: string;
  checkup_1?: object;
  checkup_2?: object;
  checkup_3?: object;
  checkup_4?: object;
  checkup?: object;
  image: string;
  initial_price: number;
  current_price: number;
  sales_volume: number;
  type: '不限' | '父母体检' | '入职体检' | '职场白领' | '个人高端' | '中青年体检';
  tag?: object;
  part_id?: number;
  rule_id?: number;
  status: number;
  md5: string;
  update_time?: Date;
  create_time: Date;
}

export type tb_goodsPk = "id";
export type tb_goodsId = tb_goods[tb_goodsPk];
export type tb_goodsOptionalAttributes = "id" | "checkup_1" | "checkup_2" | "checkup_3" | "checkup_4" | "checkup" | "sales_volume" | "tag" | "part_id" | "rule_id" | "update_time" | "create_time";
export type tb_goodsCreationAttributes = Optional<tb_goodsAttributes, tb_goodsOptionalAttributes>;

export class tb_goods extends Model<tb_goodsAttributes, tb_goodsCreationAttributes> implements tb_goodsAttributes {
  id!: number;
  code!: string;
  title!: string;
  description!: string;
  checkup_1?: object;
  checkup_2?: object;
  checkup_3?: object;
  checkup_4?: object;
  checkup?: object;
  image!: string;
  initial_price!: number;
  current_price!: number;
  sales_volume!: number;
  type!: '不限' | '父母体检' | '入职体检' | '职场白领' | '个人高端' | '中青年体检';
  tag?: object;
  part_id?: number;
  rule_id?: number;
  status!: number;
  md5!: string;
  update_time?: Date;
  create_time!: Date;


  static initModel(sequelize: Sequelize.Sequelize): typeof tb_goods {
    return tb_goods.init({
    id: {
      autoIncrement: true,
      type: DataTypes.INTEGER,
      allowNull: false,
      primaryKey: true,
      comment: "主键"
    },
    code: {
      type: DataTypes.STRING(200),
      allowNull: false,
      comment: "编号",
      unique: "unq_code"
    },
    title: {
      type: DataTypes.STRING(50),
      allowNull: false,
      comment: "商品标题"
    },
    description: {
      type: DataTypes.STRING(200),
      allowNull: false,
      comment: "商品描述"
    },
    checkup_1: {
      type: DataTypes.JSON,
      allowNull: true,
      comment: "科室检查"
    },
    checkup_2: {
      type: DataTypes.JSON,
      allowNull: true,
      comment: "实验室检查"
    },
    checkup_3: {
      type: DataTypes.JSON,
      allowNull: true,
      comment: "医技检查"
    },
    checkup_4: {
      type: DataTypes.JSON,
      allowNull: true,
      comment: "其他检查"
    },
    checkup: {
      type: DataTypes.JSON,
      allowNull: true,
      comment: "检查内容"
    },
    image: {
      type: DataTypes.STRING(300),
      allowNull: false,
      comment: "商品封面"
    },
    initial_price: {
      type: DataTypes.DECIMAL(10,2),
      allowNull: false,
      comment: "原价"
    },
    current_price: {
      type: DataTypes.DECIMAL(10,2),
      allowNull: false,
      comment: "现价"
    },
    sales_volume: {
      type: DataTypes.INTEGER,
      allowNull: false,
      defaultValue: 0,
      comment: "销量"
    },
    type: {
      type: DataTypes.ENUM('不限','父母体检','入职体检','职场白领','个人高端','中青年体检'),
      allowNull: false,
      comment: "套餐类型"
    },
    tag: {
      type: DataTypes.JSON,
      allowNull: true,
      comment: "套餐标签"
    },
    part_id: {
      type: DataTypes.TINYINT,
      allowNull: true,
      comment: "1活动专区，2热卖套餐，3新品推荐，4孝敬父母，5,白领精英"
    },
    rule_id: {
      type: DataTypes.INTEGER,
      allowNull: true,
      comment: "促销优惠规则的ID"
    },
    status: {
      type: DataTypes.BOOLEAN,
      allowNull: false,
      comment: "状态(1上架，0下架)"
    },
    md5: {
      type: DataTypes.STRING(200),
      allowNull: false,
      comment: "MD5信息"
    },
    update_time: {
      type: DataTypes.DATE,
      allowNull: true,
      comment: "最后修改时间"
    },
    create_time: {
      type: DataTypes.DATE,
      allowNull: false,
      defaultValue: Sequelize.Sequelize.literal('CURRENT_TIMESTAMP'),
      comment: "创建时间"
    }
  }, {
    sequelize,
    tableName: 'tb_goods',
    timestamps: false,
    indexes: [
      {
        name: "PRIMARY",
        unique: true,
        using: "BTREE",
        fields: [
          { name: "id" },
        ]
      },
      {
        name: "unq_code",
        unique: true,
        using: "BTREE",
        fields: [
          { name: "code" },
        ]
      },
      {
        name: "idx_type",
        using: "BTREE",
        fields: [
          { name: "type" },
        ]
      },
      {
        name: "idx_status",
        using: "BTREE",
        fields: [
          { name: "status" },
        ]
      },
    ]
  });
  }
}
