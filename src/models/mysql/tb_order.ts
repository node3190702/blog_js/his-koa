import * as Sequelize from 'sequelize';
import { DataTypes, Model, Optional } from 'sequelize';

export interface tb_orderAttributes {
  id: number;
  customer_id: number;
  goods_id: number;
  snapshot_id: string;
  goods_title: string;
  goods_price: number;
  number: number;
  amount: number;
  goods_image: string;
  goods_description: string;
  out_trade_no: string;
  transaction_id?: string;
  out_refund_no?: string;
  status: number;
  create_date: string;
  create_time: Date;
  refund_date?: string;
  refund_time?: Date;
}

export type tb_orderPk = "id";
export type tb_orderId = tb_order[tb_orderPk];
export type tb_orderOptionalAttributes = "id" | "transaction_id" | "out_refund_no" | "create_time" | "refund_date" | "refund_time";
export type tb_orderCreationAttributes = Optional<tb_orderAttributes, tb_orderOptionalAttributes>;

export class tb_order extends Model<tb_orderAttributes, tb_orderCreationAttributes> implements tb_orderAttributes {
  id!: number;
  customer_id!: number;
  goods_id!: number;
  snapshot_id!: string;
  goods_title!: string;
  goods_price!: number;
  number!: number;
  amount!: number;
  goods_image!: string;
  goods_description!: string;
  out_trade_no!: string;
  transaction_id?: string;
  out_refund_no?: string;
  status!: number;
  create_date!: string;
  create_time!: Date;
  refund_date?: string;
  refund_time?: Date;


  static initModel(sequelize: Sequelize.Sequelize): typeof tb_order {
    return tb_order.init({
    id: {
      autoIncrement: true,
      type: DataTypes.INTEGER,
      allowNull: false,
      primaryKey: true,
      comment: "主键"
    },
    customer_id: {
      type: DataTypes.INTEGER,
      allowNull: false,
      comment: "客户ID"
    },
    goods_id: {
      type: DataTypes.INTEGER,
      allowNull: false,
      comment: "商品ID"
    },
    snapshot_id: {
      type: DataTypes.STRING(200),
      allowNull: false,
      comment: "商品快照ID"
    },
    goods_title: {
      type: DataTypes.STRING(50),
      allowNull: false,
      comment: "商品标题"
    },
    goods_price: {
      type: DataTypes.DECIMAL(10,2),
      allowNull: false,
      comment: "商品价格"
    },
    number: {
      type: DataTypes.INTEGER,
      allowNull: false,
      comment: "购买数量"
    },
    amount: {
      type: DataTypes.DECIMAL(10,2),
      allowNull: false,
      comment: "付款金额"
    },
    goods_image: {
      type: DataTypes.STRING(300),
      allowNull: false,
      comment: "商品封面"
    },
    goods_description: {
      type: DataTypes.STRING(200),
      allowNull: false,
      comment: "商品描述"
    },
    out_trade_no: {
      type: DataTypes.CHAR(32),
      allowNull: false,
      comment: "订单流水号",
      unique: "unq_out_trade_no"
    },
    transaction_id: {
      type: DataTypes.CHAR(32),
      allowNull: true,
      comment: "付款单ID",
      unique: "unq_transaction_id"
    },
    out_refund_no: {
      type: DataTypes.CHAR(64),
      allowNull: true,
      comment: "退款单流水号"
    },
    status: {
      type: DataTypes.TINYINT,
      allowNull: false,
      comment: "订单状态。1未付款，2已关闭，3已付款，4已退款，5已预约，6已结束"
    },
    create_date: {
      type: DataTypes.DATEONLY,
      allowNull: false,
      comment: "下单日期"
    },
    create_time: {
      type: DataTypes.DATE,
      allowNull: false,
      defaultValue: Sequelize.Sequelize.literal('CURRENT_TIMESTAMP'),
      comment: "下单日期时间"
    },
    refund_date: {
      type: DataTypes.DATEONLY,
      allowNull: true,
      comment: "退款日期"
    },
    refund_time: {
      type: DataTypes.DATE,
      allowNull: true,
      comment: "退款日期时间"
    }
  }, {
    sequelize,
    tableName: 'tb_order',
    timestamps: false,
    indexes: [
      {
        name: "PRIMARY",
        unique: true,
        using: "BTREE",
        fields: [
          { name: "id" },
        ]
      },
      {
        name: "unq_prepay_id",
        unique: true,
        using: "BTREE",
        fields: [
          { name: "out_trade_no" },
        ]
      },
      {
        name: "unq_out_trade_no",
        unique: true,
        using: "BTREE",
        fields: [
          { name: "out_trade_no" },
        ]
      },
      {
        name: "unq_transaction_id",
        unique: true,
        using: "BTREE",
        fields: [
          { name: "transaction_id" },
        ]
      },
      {
        name: "idx_customer_id",
        using: "BTREE",
        fields: [
          { name: "customer_id" },
        ]
      },
      {
        name: "idx_goods_id",
        using: "BTREE",
        fields: [
          { name: "goods_id" },
        ]
      },
      {
        name: "idx_snapshot_id",
        using: "BTREE",
        fields: [
          { name: "snapshot_id" },
        ]
      },
    ]
  });
  }
}
