import * as Sequelize from 'sequelize';
import { DataTypes, Model, Optional } from 'sequelize';

export interface tb_appointmentAttributes {
  id: number;
  uuid: string;
  order_id: number;
  date: string;
  name: string;
  sex: string;
  pid: string;
  birthday: string;
  tel: string;
  mailing_address: string;
  company?: string;
  status: number;
  checkin_time?: Date;
  create_time: Date;
}

export type tb_appointmentPk = "id";
export type tb_appointmentId = tb_appointment[tb_appointmentPk];
export type tb_appointmentOptionalAttributes = "id" | "company" | "checkin_time" | "create_time";
export type tb_appointmentCreationAttributes = Optional<tb_appointmentAttributes, tb_appointmentOptionalAttributes>;

export class tb_appointment extends Model<tb_appointmentAttributes, tb_appointmentCreationAttributes> implements tb_appointmentAttributes {
  id!: number;
  uuid!: string;
  order_id!: number;
  date!: string;
  name!: string;
  sex!: string;
  pid!: string;
  birthday!: string;
  tel!: string;
  mailing_address!: string;
  company?: string;
  status!: number;
  checkin_time?: Date;
  create_time!: Date;


  static initModel(sequelize: Sequelize.Sequelize): typeof tb_appointment {
    return tb_appointment.init({
    id: {
      autoIncrement: true,
      type: DataTypes.INTEGER,
      allowNull: false,
      primaryKey: true,
      comment: "主键"
    },
    uuid: {
      type: DataTypes.CHAR(32),
      allowNull: false,
      comment: "UUID",
      unique: "unq_uuid"
    },
    order_id: {
      type: DataTypes.INTEGER,
      allowNull: false,
      comment: "订单编号"
    },
    date: {
      type: DataTypes.DATEONLY,
      allowNull: false,
      comment: "预约日期"
    },
    name: {
      type: DataTypes.STRING(10),
      allowNull: false,
      comment: "姓名"
    },
    sex: {
      type: DataTypes.CHAR(1),
      allowNull: false,
      comment: "性别"
    },
    pid: {
      type: DataTypes.CHAR(18),
      allowNull: false,
      comment: "身份证号"
    },
    birthday: {
      type: DataTypes.DATEONLY,
      allowNull: false,
      comment: "出生日期"
    },
    tel: {
      type: DataTypes.CHAR(11),
      allowNull: false,
      comment: "电话号码"
    },
    mailing_address: {
      type: DataTypes.STRING(200),
      allowNull: false,
      comment: "体检报告邮寄地址"
    },
    company: {
      type: DataTypes.STRING(100),
      allowNull: true,
      comment: "企业名称"
    },
    status: {
      type: DataTypes.TINYINT,
      allowNull: false,
      comment: "状态。1未签到，2已签到，3已结束，4已关闭"
    },
    checkin_time: {
      type: DataTypes.DATE,
      allowNull: true,
      comment: "签到时间"
    },
    create_time: {
      type: DataTypes.DATE,
      allowNull: false,
      defaultValue: Sequelize.Sequelize.literal('CURRENT_TIMESTAMP'),
      comment: "创建时间"
    }
  }, {
    sequelize,
    tableName: 'tb_appointment',
    timestamps: false,
    indexes: [
      {
        name: "PRIMARY",
        unique: true,
        using: "BTREE",
        fields: [
          { name: "id" },
        ]
      },
      {
        name: "unq_uuid",
        unique: true,
        using: "BTREE",
        fields: [
          { name: "uuid" },
        ]
      },
      {
        name: "idx_order_id",
        using: "BTREE",
        fields: [
          { name: "order_id" },
        ]
      },
      {
        name: "idx_date",
        using: "BTREE",
        fields: [
          { name: "date" },
        ]
      },
      {
        name: "idx_pid",
        using: "BTREE",
        fields: [
          { name: "pid" },
        ]
      },
      {
        name: "idx_tel",
        using: "BTREE",
        fields: [
          { name: "tel" },
        ]
      },
      {
        name: "idx_status",
        using: "BTREE",
        fields: [
          { name: "status" },
        ]
      },
    ]
  });
  }
}
