import * as Sequelize from 'sequelize';
import { DataTypes, Model, Optional } from 'sequelize';

export interface tb_roleAttributes {
  id: number;
  role_name: string;
  permissions: object;
  desc?: string;
  default_permissions?: object;
  systemic?: number;
}

export type tb_rolePk = "id";
export type tb_roleId = tb_role[tb_rolePk];
export type tb_roleOptionalAttributes = "id" | "desc" | "default_permissions" | "systemic";
export type tb_roleCreationAttributes = Optional<tb_roleAttributes, tb_roleOptionalAttributes>;

export class tb_role extends Model<tb_roleAttributes, tb_roleCreationAttributes> implements tb_roleAttributes {
  id!: number;
  role_name!: string;
  permissions!: object;
  desc?: string;
  default_permissions?: object;
  systemic?: number;


  static initModel(sequelize: Sequelize.Sequelize): typeof tb_role {
    return tb_role.init({
    id: {
      autoIncrement: true,
      type: DataTypes.INTEGER.UNSIGNED,
      allowNull: false,
      primaryKey: true,
      comment: "主键"
    },
    role_name: {
      type: DataTypes.STRING(200),
      allowNull: false,
      comment: "角色名称",
      unique: "unq_role_name"
    },
    permissions: {
      type: DataTypes.JSON,
      allowNull: false,
      comment: "权限集合"
    },
    desc: {
      type: DataTypes.STRING(200),
      allowNull: true,
      comment: "描述"
    },
    default_permissions: {
      type: DataTypes.JSON,
      allowNull: true,
      comment: "系统角色内置权限"
    },
    systemic: {
      type: DataTypes.BOOLEAN,
      allowNull: true,
      defaultValue: 0,
      comment: "是否为系统内置角色"
    }
  }, {
    sequelize,
    tableName: 'tb_role',
    timestamps: false,
    indexes: [
      {
        name: "PRIMARY",
        unique: true,
        using: "BTREE",
        fields: [
          { name: "id" },
        ]
      },
      {
        name: "unq_role_name",
        unique: true,
        using: "BTREE",
        fields: [
          { name: "role_name" },
        ]
      },
    ]
  });
  }
}
