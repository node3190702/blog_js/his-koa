import  { Sequelize } from "sequelize";
import { config } from "../../common/configuration/mysql.configuration";

import { tb_action } from "./tb_action"
import { tb_appointment } from "./tb_appointment"
import { tb_appointment_restriction } from "./tb_appointment_restriction"
import { tb_checkup_report } from "./tb_checkup_report"
import { tb_customer } from "./tb_customer"
import { tb_customer_im } from "./tb_customer_im"
import { tb_customer_location } from "./tb_customer_location"
import { tb_dept } from "./tb_dept"
import { tb_flow_regulation } from "./tb_flow_regulation"
import { tb_goods } from "./tb_goods"
import { tb_module } from "./tb_module"
import { tb_order } from "./tb_order"
import { tb_permission } from "./tb_permission"
import { tb_role } from "./tb_role"
import { tb_system } from "./tb_system"
import { tb_user } from "./tb_user"
import { tb_rule } from './tb_rule'


const sequelize = new Sequelize('', '', '', {
  dialect: config.DB.DB_TYPE,
  database: config.DB.DB_DATABASE,
  username: config.DB.DB_USERNAME,
  password: config.DB.DB_PASSWORD,
  port: config.PORT,
  host: config.DB.DB_HOST
})

const models = [tb_action, tb_rule,
  tb_module, tb_user, tb_appointment, tb_appointment_restriction, tb_checkup_report,
  tb_customer, tb_customer_im, tb_customer_location,tb_dept, tb_flow_regulation, tb_goods, tb_order,
  tb_permission, tb_role, tb_system]

models.forEach( (model) => {
  model.initModel(sequelize)
})

export default sequelize
