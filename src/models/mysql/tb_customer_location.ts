import * as Sequelize from 'sequelize';
import { DataTypes, Model, Optional } from 'sequelize';

export interface tb_customer_locationAttributes {
  id: number;
  customer_id: number;
  blue_uuid: string;
  place_id: number;
  create_time: Date;
}

export type tb_customer_locationPk = "id";
export type tb_customer_locationId = tb_customer_location[tb_customer_locationPk];
export type tb_customer_locationCreationAttributes = tb_customer_locationAttributes;

export class tb_customer_location extends Model<tb_customer_locationAttributes, tb_customer_locationCreationAttributes> implements tb_customer_locationAttributes {
  id!: number;
  customer_id!: number;
  blue_uuid!: string;
  place_id!: number;
  create_time!: Date;


  static initModel(sequelize: Sequelize.Sequelize): typeof tb_customer_location {
    return tb_customer_location.init({
    id: {
      type: DataTypes.INTEGER,
      allowNull: false,
      primaryKey: true,
      comment: "主键"
    },
    customer_id: {
      type: DataTypes.INTEGER,
      allowNull: false,
      comment: "客户ID"
    },
    blue_uuid: {
      type: DataTypes.STRING(64),
      allowNull: false,
      comment: "蓝牙信标UUID"
    },
    place_id: {
      type: DataTypes.INTEGER,
      allowNull: false,
      comment: "科室ID"
    },
    create_time: {
      type: DataTypes.DATE,
      allowNull: false,
      comment: "日期时间"
    }
  }, {
    sequelize,
    tableName: 'tb_customer_location',
    timestamps: false,
    indexes: [
      {
        name: "PRIMARY",
        unique: true,
        using: "HASH",
        fields: [
          { name: "id" },
        ]
      },
    ]
  });
  }
}
