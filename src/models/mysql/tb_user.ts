import * as Sequelize from 'sequelize';
import { DataTypes, Model, Optional } from 'sequelize';

export interface tb_userAttributes {
  id: number;
  username: string;
  password: string;
  open_id?: string;
  photo?: string;
  name: string;
  sex: '男' | '女';
  tel: string;
  email: string;
  hiredate?: string;
  role: object;
  root: number;
  dept_id?: number;
  status: number;
  create_time: Date;
}

export type tb_userPk = "id";
export type tb_userId = tb_user[tb_userPk];
export type tb_userOptionalAttributes = "id" | "open_id" | "photo" | "hiredate" | "root" | "dept_id" | "create_time";
export type tb_userCreationAttributes = Optional<tb_userAttributes, tb_userOptionalAttributes>;

export class tb_user extends Model<tb_userAttributes, tb_userCreationAttributes> implements tb_userAttributes {
  id!: number;
  username!: string;
  password!: string;
  open_id?: string;
  photo?: string;
  name!: string;
  sex!: '男' | '女';
  tel!: string;
  email!: string;
  hiredate?: string;
  role!: object;
  root!: number;
  dept_id?: number;
  status!: number;
  create_time!: Date;


  static initModel(sequelize: Sequelize.Sequelize): typeof tb_user {
    return tb_user.init({
    id: {
      autoIncrement: true,
      type: DataTypes.INTEGER,
      allowNull: false,
      primaryKey: true,
      comment: "主键"
    },
    username: {
      type: DataTypes.STRING(200),
      allowNull: false,
      comment: "用户名",
      unique: "unq_username"
    },
    password: {
      type: DataTypes.STRING(200),
      allowNull: false,
      comment: "密码"
    },
    open_id: {
      type: DataTypes.STRING(200),
      allowNull: true,
      comment: "长期授权字符串",
      unique: "unq_open_id"
    },
    photo: {
      type: DataTypes.STRING(200),
      allowNull: true,
      comment: "头像网址"
    },
    name: {
      type: DataTypes.STRING(20),
      allowNull: false,
      comment: "姓名"
    },
    sex: {
      type: DataTypes.ENUM('男','女'),
      allowNull: false,
      comment: "性别"
    },
    tel: {
      type: DataTypes.CHAR(11),
      allowNull: false,
      comment: "手机号码"
    },
    email: {
      type: DataTypes.STRING(200),
      allowNull: false,
      comment: "邮箱"
    },
    hiredate: {
      type: DataTypes.DATEONLY,
      allowNull: true,
      comment: "入职日期"
    },
    role: {
      type: DataTypes.JSON,
      allowNull: false,
      comment: "角色"
    },
    root: {
      type: DataTypes.BOOLEAN,
      allowNull: false,
      defaultValue: 0,
      comment: "是否是超级管理员"
    },
    dept_id: {
      type: DataTypes.INTEGER,
      allowNull: true,
      comment: "部门编号"
    },
    status: {
      type: DataTypes.TINYINT,
      allowNull: false,
      comment: "状态"
    },
    create_time: {
      type: DataTypes.DATE,
      allowNull: false,
      defaultValue: Sequelize.Sequelize.literal('CURRENT_TIMESTAMP'),
      comment: "创建时间"
    }
  }, {
    sequelize,
    tableName: 'tb_user',
    timestamps: false,
    indexes: [
      {
        name: "PRIMARY",
        unique: true,
        using: "BTREE",
        fields: [
          { name: "id" },
        ]
      },
      {
        name: "unq_username",
        unique: true,
        using: "BTREE",
        fields: [
          { name: "username" },
        ]
      },
      {
        name: "unq_open_id",
        unique: true,
        using: "BTREE",
        fields: [
          { name: "open_id" },
        ]
      },
      {
        name: "idx_dept_id",
        using: "BTREE",
        fields: [
          { name: "dept_id" },
        ]
      },
      {
        name: "idx_status",
        using: "BTREE",
        fields: [
          { name: "status" },
        ]
      },
    ]
  });
  }
}
