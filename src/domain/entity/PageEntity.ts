export class PageEntity {
    /**
     * 总记录数
     */
     totalCount?: number;

    /**
     * 每页显示几条记录
     */
     pageSize?: number;

    /**
     * 总页数
     */
     totalPage?: number;

    /**
     * 当前页数
     */
     pageIndex?: number;

    /**
     * 分页数据
     */
     list?: any[];

     constructor(list:  any[] = [], totalCount: number, pageSize: number, pageIndex: number) {
        this.pageSize = pageSize;
        this.pageIndex = pageIndex;
        this.totalCount = totalCount;
        this.totalPage = Math.ceil(totalCount / pageSize)
         this.list = list;
     }
}
