export class RoleEntity {
    id?: number;
    role_name?: string;
    permissions?: object;
    desc?: string;
    default_permissions?: object;
    systemic?: number;
}
