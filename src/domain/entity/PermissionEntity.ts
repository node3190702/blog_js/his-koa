export class PermissionEntity {
    id?: number;
    permission_name?: string;
    module_id?: number;
    action_id?: number;
}
