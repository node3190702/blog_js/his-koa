export class UserEntity {
    id?: number;
    username?: string;
    open_id?: string;
    photo?: string;
    name?: '男' | '女';
    tel?: string;
    email?: string;
    hiredate?: string;
    role?: object;
    root?: number;
    dept_id?: number;
    status?: number;
    create_time!: Date;
}
