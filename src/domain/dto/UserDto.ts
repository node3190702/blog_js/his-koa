export interface LoginDto {
    username: string;
    password: string;
}


export interface UserPasswordDTO {
    password: string;
    newPassword: string;
}

export interface searchDTO {
    "page": number,
    "length": number,
    "name": string,
    "sex": string,
    "role": string,
    "deptId": number,
    "status": number
}
