import Sequelize from '../../models/mysql/init-models'

class ApiService {
    /**
     * 查询用户权限
     * @param userId
     */
    async searchPermission(userId: string) {
        const sql =
            'select p.permission_name, count(*) as flag ' +
            'from tb_user u ' +
            'join his.tb_role r on JSON_CONTAINS(u.role, CAST(r.id AS CHAR ))' +
            'join his.tb_permission p on JSON_CONTAINS(r.permissions, CAST(p.id AS CHAR)) where u.id = :userId AND u.status = 1;'
            const [result] = await Sequelize.query(sql, {
                replacements: {userId: userId }
            })
        return result[0];
    }
}




export default new ApiService();


