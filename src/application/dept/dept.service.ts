import {tb_dept} from "../../models/mysql/tb_dept";

class DeptService {
    async getAllDepts() {
        return await tb_dept.findAll({
            attributes: ['id','dept_name'],
            order: ['id']
        })
    }
}


export default new DeptService()
