import {Context} from "koa";
import DeptService from "./dept.service";
import {Result} from "../../common/result/result";

class DeptController {
    getDeptList = async (ctx: Context, next: Function) =>  {
        const result = await DeptService.getAllDepts()
        ctx.body = Result.success(result)
    }
}

export default new DeptController()
