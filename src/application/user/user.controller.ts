import { Context } from "node:vm";
import { LoginDto, searchDTO } from "../../domain/dto/UserDto";
import MisService from "./user.service";
import { Result } from "../../common/result/result";
import { ApiException } from "../../common/exception/api.exception";
import HttpStatusCode from "../../common/constant/http-code.constants";
import { MessageConstant } from "../../common/constant/message.constant";
import ApiService from "../common/Api.service";
import { LoginVO } from "../../domain/VO/LoginVO";
import Jwt from "../../utils/Jwt.util";
import { jwtConfig } from "../../common/configuration/jwt.configuration";
import Redis from "../../models/redis/Redis";
import {PageEntity} from "../../domain/entity/PageEntity";

class UserController {

    /**
     * 用户登录
     * @param ctx
     * @param next
     */
    login = async (ctx: Context, next: Function) => {
        const userLogin: LoginDto = ctx.request.body

        const result: any = await MisService.login(userLogin)
        if (!result.flag) throw new ApiException(HttpStatusCode.BAD_REQUEST, MessageConstant.LOGIN_USERINFO_NOT_CORRECT)

        const perResult: any = await ApiService.searchPermission(result.id)
        if (!perResult.flag)throw new ApiException(HttpStatusCode.BAD_REQUEST, MessageConstant.USER_PERMISSION_NOT_EXIST)

        // 生成token
        const key = `${jwtConfig.redis_key}_${result.id}`
        await Redis.select(jwtConfig.redis_db)
        let token = await Redis.get(key)
        if (!token) {
            token = Jwt.getJwtToken( { id: result.id })
            await Redis.set(key, token, 'EX', jwtConfig.redis_expire)
        }

        const LoginResultVO = new LoginVO()
        LoginResultVO.token = token
        LoginResultVO.permissions = perResult.permission_name

        ctx.body = Result.put('result', true).success(LoginResultVO)
     }

    /**
     * 用户登出
     * @param ctx
     * @param next
     */
     logout = async (ctx: Context, next: Function) => {
        const token = ctx.headers.access_token
        const userInfo =  Jwt.TokenToUserInfo(token)
         await Redis.del(`${jwtConfig.redis_key}_${userInfo.id}`)
         ctx.set('access_token', '')
         ctx.body = Result.put('result', true).success()
     }

    /**
     * 修改用户密码
     * @param ctx
     * @param next
     */
    updatePassword = async (ctx: Context, next: Function) => {
         const { password, newPassword } = ctx.request.body
        const uid = ctx.userId
        const userResult = await MisService.updateUserPasswordById(uid, password, newPassword )
        ctx.body = Result.put('result', true).success({
            row: userResult
        })
     }

    searchByPage = async (ctx: Context, next: Function) => {
        const searchParams: searchDTO = ctx.request.body
        const { result, count } = await MisService.searchByPage(searchParams)
        const page = new PageEntity(result, count, searchParams.page, searchParams.length)
        ctx.body = Result.success(page)
     }
}

export default new UserController();
