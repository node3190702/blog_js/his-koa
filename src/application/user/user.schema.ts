import Joi from 'joi'

export const MisLoginSchema = Joi.object({
        username: Joi.string()
            .required() // 确保用户名不为空
            .min(4) // 用户名最小长度为4
            .max(16) // 用户名最大长度为16
            .pattern(/^[a-zA-Z0-9_-]+$/) // 用户名只能包含字母、数字、下划线或短划线
            .messages({
                'string.base': '用户名不能为空',
                'string.min': '用户名长度不能小于4个字符',
                'string.max': '用户名长度不能大于16个字符',
                'string.pattern.base': '用户名格式错误'
        }),
        password: Joi.string()
            .required() // 确保密码不为空
            .min(6) // 密码最小长度为6
            .max(18) // 密码最大长度为18
            .pattern(/^[a-zA-Z0-9_-]+$/) // 密码只能包含字母、数字、下划线或短划线
                .messages({
                'string.base': '密码不能为空',
                'string.min': '密码长度不能小于6个字符',
                'string.max': '密码长度不能大于18个字符',
                'string.pattern.base': '密码格式错误'
        })
})


export const MisUpdatePasswordSchema = Joi.object({
        password: Joi.string()
            .required()
            .min(6)
            .max(20)
            .message('旧密码必须是一个6到20个字符的字符串'),
        newPassword: Joi.string()
            .required()
            .min(6)
            .max(20)
            .message('新密码必须是一个6到20个字符的字符串'),
});

export const MisSearchUserByPageFormSchema = Joi.object({
        page: Joi.number().integer().min(1).required().messages({
                'number.base': 'page必须是一个数字',
                'number.integer': 'page必须是一个整数',
                'number.min': 'page必须大于等于1',
                'any.required': 'page不能为空',
        }),
        length: Joi.number().integer().min(10).max(50).required().messages({
                'number.base': 'length必须是一个数字',
                'number.integer': 'length必须是一个整数',
                'number.min': 'length必须大于等于10',
                'number.max': 'length必须小于等于50',
                'any.required': 'length不能为空',
        }),
        name: Joi.string().regex(/^[\u4e00-\u9fa5]{1,10}$/).allow('').optional().messages({
                'string.pattern.base': 'name必须是1到10个中文字符',
        }),
        sex: Joi.string().valid('男', '女').allow('').optional().messages({
                'string.valid': 'sex只能是男或女',
        }),
        role: Joi.string().regex(/^[a-zA-Z0-9\u4e00-\u9fa5]{2,10}$/).allow('').optional().messages({
                'string.pattern.base': 'role必须是2到10个字母、数字或中文字符',
        }),
        deptId: Joi.number().integer().min(1).allow(null).optional().messages({
                'number.base': 'deptId必须是一个数字',
                'number.integer': 'deptId必须是一个整数',
                'number.min': 'deptId必须大于等于1',
        }),
        status: Joi.number().integer().min(1).allow(null).optional().messages({
                'number.base': 'status必须是一个数字',
                'number.integer': 'status必须是一个整数',
                'number.min': 'status必须大于等于1',
        }),
});
