import { LoginDto, searchDTO } from "../../domain/dto/UserDto";
import CryptoUtil from "../../utils/Crypto.util";
import Sequelize from "../../models/mysql/init-models";
import { tb_user } from "../../models/mysql/tb_user";
import { ApiException } from "../../common/exception/api.exception";
import HttpStatusCode from "../../common/constant/http-code.constants";
import { MessageConstant } from "../../common/constant/message.constant";


class UserService {

    /**
     * 用户登录
     * @param userLogin
     */
    async login(userLogin: LoginDto) {
        let { username, password } = userLogin
        let start = CryptoUtil.MD5(username).substring(0, 6)
        let end = CryptoUtil.MD5(password).substring(username.length - 3)
        password = CryptoUtil.MD5(`${start}${password}${end}`).toUpperCase()
        userLogin.password = password

        const sql = `select id, count(id) as flag from tb_user where username = :username and password = :password limit 1;`
            const [result] = await Sequelize.query(sql, {
                replacements: {
                    username: userLogin.username,
                    password: userLogin.password
                }
            })
        return  result[0]
    }

    /**
     * 更新用户密码
     * @param userId
     * @param oldValue
     * @param newValue
     */
    async updateUserPasswordById(userId: number, oldValue: any, newValue: any ) {
        const [userInfo] = await Sequelize.query(`select username, count(username) as num from tb_user where id = :id  limit 1;`, {
            replacements: {
                id: userId
            }
        })

        const { username, num }: any = userInfo[0]
        if ( !num ) throw new ApiException(HttpStatusCode.BAD_REQUEST, MessageConstant.LOGIN_USERE_NOT_EXIST)


        let start = CryptoUtil.MD5(username).substring(0, 6)
        let endNew = CryptoUtil.MD5(newValue).substring(username.length - 3)
        let endOld = CryptoUtil.MD5(oldValue).substring(username.length - 3)

        const newPassword = CryptoUtil.MD5(`${start}${newValue}${endNew}`).toUpperCase()
        const password = CryptoUtil.MD5(`${start}${oldValue}${endOld}`).toUpperCase()

        console.log(password)
        console.log(newPassword)
        const [row] = await tb_user.update({
            password: newPassword // 设置新的密码
        }, {
            where: {
                id: userId, // 指定要更新的用户的 ID
                password: password // 指定当前的（旧的）密码
            }
        });
        return row
    }

    /**
     * 分页查询
     * @param searchParams
     */
    async searchByPage(searchParams: searchDTO) {
        const sql  = `
                SELECT u.id, u.sex, u.tel, u.email, d.dept_name AS dept, u.hiredate, u.root, u.status, GROUP_CONCAT(r.role_name) AS role_list
                FROM tb_user u JOIN tb_role r ON JSON_CONTAINS(u.role, CAST(r.id AS CHAR)) LEFT JOIN tb_dept d ON u.dept_id = d.id
                WHERE (:name IS NULL OR u.name LIKE CONCAT('%', :name, '%'))
                    AND (:sex IS NULL OR u.sex = :sex)
                    AND (:role IS NULL OR JSON_CONTAINS(u.role, CAST(:role AS CHAR)))
                    AND (:deptId IS NULL OR u.dept_id = :deptId)
                    AND (:status IS NULL OR u.status = :status)
                GROUP BY u.id ORDER BY u.id ASC LIMIT :start, :length;`
        const countSQL = `select count(*) as count  from ( select distinct u.id from tb_user u join tb_role r on JSON_CONTAINS(u.role, CAST(r.id AS CHAR))
                                                    where (:name IS NULL OR u.name LIKE CONCAT('%', :name, '%'))
                                                      AND (:role IS NULL OR JSON_CONTAINS(u.role, CAST(:role AS CHAR)))
                                                      AND (:dept_id IS NULL OR u.dept_id = :dept_id)
                                                      AND (:status IS NULL OR u.status = :status))  as temp`

        let start: number = searchParams.length * (searchParams.page - 1)
        const [result] = await Sequelize.query(sql, {
            replacements: {
                name: searchParams.name ?? null,
                sex: searchParams.sex ?? null,
                role: searchParams.role ?? null,
                deptId: searchParams.deptId ?? null,
                status: searchParams.status ?? null,
                start: start,
                length: searchParams.length
            }
        })
        const [count]: any = await Sequelize.query(countSQL, {
            replacements: {
                name: searchParams.name ?? null,
                role: searchParams.role ?? null,
                dept_id: searchParams.deptId ?? null,
                status: searchParams.status ?? null
            }
        })
        return {
            result,
            count: count[0].count
        }
    }
}



export default new UserService();


// const sql  = `
//                 SELECT u.id, u.sex, u.tel, u.email, d.dept_name AS dept, u.hiredate, u.root, u.status, GROUP_CONCAT(r.role_name) AS role_list
//                 FROM tb_user u JOIN tb_role r ON JSON_CONTAINS(u.role, CAST(r.id AS CHAR)) LEFT JOIN tb_dept d ON u.dept_id = d.id
//                 WHERE (:name IS NULL OR u.name LIKE CONCAT('%', :name, '%'))
//                     AND (:sex IS NULL OR u.sex = :sex)
//                     AND (:role IS NULL OR JSON_CONTAINS(u.role, CAST(:role AS CHAR)))
//                     AND (:deptId IS NULL OR u.deptId = :deptId)
//                     AND (:status IS NULL OR u.status = :status)
//                 GROUP BY u.id ORDER BY u.id ASC LIMIT :start, :length;`
// let start: number = searchParams.length * (searchParams.page - 1)
//
// const [result] = await Sequelize.query(sql, {
//     replacements: {
//         name: searchParams.name ?? null,
//         sex: searchParams.sex ?? null,
//         role: searchParams.role ?? null,
//         deptId: searchParams.deptId ?? null,
//         status: searchParams.status ?? null,
//         start: start,
//         length: searchParams.length
//     }
// })
// return result[0]
