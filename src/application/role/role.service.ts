import {tb_role} from "../../models/mysql/tb_role";

class RoleService {
    async  getRole() {
         return await tb_role.findAll({
             attributes: ['id', 'role_name'],
             order: ['id']
         })
     }
}

export default new RoleService();
