import { Context } from "koa";
import RoleService from "./role.service";
import { Result } from "../../common/result/result";

class RoleController {
    searchAllRole = async (ctx: Context, next: Function) => {
        const result = await RoleService.getRole()
        ctx.body = Result.success(result)
     }
}


export default new RoleController();
