import { Validator } from "../../common/validate/validator";
import { MisLoginSchema, MisSearchUserByPageFormSchema, MisUpdatePasswordSchema } from "../../application/user/user.schema";


/**
 * 登录验证
 */
export const MisLoginValidate = Validator.getInstance(MisLoginSchema, 0).validate

/**
 * 用户更新密码验证
 */
export const MisUpdateValidate = Validator.getInstance(MisUpdatePasswordSchema, 0).validate

/**
 * 用户分页查询校验
 */
export const MisSearchUserByPageFormValidate = Validator.getInstance(MisSearchUserByPageFormSchema, 0).validate

