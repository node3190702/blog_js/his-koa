import { Context } from "koa";
import { ApiException } from "../common/exception/api.exception";
import HttpStatusCode from "../common/constant/http-code.constants";
import { MessageConstant } from "../common/constant/message.constant";
import jsonwebtoken from "jsonwebtoken";
import { jwtConfig } from "../common/configuration/jwt.configuration";
import Redis from "../models/redis/Redis";

export const TokenValidate = async (ctx: Context, next: Function) => {
    let token = ctx.headers.access_token as string;

    // 检查token是否提供
    if (!token) throw new ApiException(HttpStatusCode.BAD_REQUEST, MessageConstant.NOT_TOKEN_PROVIDE)

    // 解析token获取信息
    const info: any = jsonwebtoken.decode(token, { complete: true })
    if (!info || !info.payload || !info.payload.id)  throw new ApiException(HttpStatusCode.BAD_REQUEST, MessageConstant.INVALID_TOKEN)
    const userId = info.payload.id

    // 验证token是否有效
    const key = `${jwtConfig.redis_key}_${userId}`
    await Redis.select(jwtConfig.redis_db)
    const redisToken = await Redis.get(key)

    if (!redisToken || redisToken !== token) throw new ApiException(HttpStatusCode.BAD_REQUEST, MessageConstant.INVALID_TOKEN);

    try {
        jsonwebtoken.verify(token, jwtConfig.secret)
    } catch (e: any) {
        await Redis.del(key)
        throw new ApiException(HttpStatusCode.BAD_REQUEST, e.message)
    }

    // token自动续期
    // const newToken = Jwt.getJwtToken( { id: userId })
    // await Redis.select(jwtConfig.redis_db)
    // await Redis.set(key, newToken, 'EX', jwtConfig.redis_expire)
    // ctx.set('access_token', newToken)
    ctx.userId = userId

    await  next()
}
