import { Context } from 'koa'; // 假设您使用的是Koa框架
import { ValidationError } from 'joi'; // 确保从joi库中正确导入ValidationError
import { ApiException } from '../exception/api.exception';
import HttpStatusCode from "../constant/http-code.constants";

export class Validator {

    private static instance: Validator | null = null;

    /**
     * 校验规则
     * @private
     */
    private schema: any;

    /**
     * 0: post请求参数
     * 1: get请求体验证
     * 2: 路径参数验证
     * @private
     */
    private TypeData: 0 | 1 | 2;

     constructor(schema: any, TypeData: 0 | 1 | 2) {
        this.schema = schema;
        this.TypeData = TypeData;
    }

    /**
     * 生成校验单例
     * @param schema
     * @param TypeData
     */
    public static getInstance(schema: any, TypeData: 0 | 1 | 2): Validator {
        return new Validator(schema, TypeData)
    }

    // 执行验证的方法
    public validate = async (ctx: Context, next: Function) => {
        let resultParams: any = null;
        switch (this.TypeData){
            case 0:
                resultParams = ctx.request.body;
                break;
            case 1:
                resultParams = ctx.request.query;
                break;
            case 2:
                resultParams = ctx.params;
                break;
        }

        try {
            // 验证请求体是否符合给定的模式
            await this.schema.validateAsync(resultParams);
        } catch (e) {
            // 捕获验证错误并抛出自定义的ApiException
            if (e instanceof ValidationError) {
                // 可以选择传递ValidationError的详细信息给ApiException
                throw new ApiException(HttpStatusCode.BAD_REQUEST, e.message);
            } else {
                throw e;
            }
        }
        await next();
    }
}
