import {toNumber} from "lodash";

interface JwtConfiguration {
    secret: string;
    expiresIn: string; // 例如：'1h'
    redis_key: string;
    redis_db: string
    redis_expire: number;
}


export  const jwtConfig: JwtConfiguration = {
    secret: process.env.JWT_SECRET || 'your-secret-key',
    expiresIn: process.env.JWT_EXPIRES_IN || '1h',
    redis_key: process.env.JWT_REDIS_PREFIX || 'your-redis-key',
    redis_db: process.env.JWT_REDIS_DB || '0',
    redis_expire: toNumber(process.env.JWT_REDIS_EXPIRES_IN) || 3600
}
