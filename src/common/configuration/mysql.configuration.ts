import { toNumber } from 'lodash'
import { Dialect } from 'sequelize'
export interface MysqlConfig {
    PORT: number,
    DB: {
        DB_TYPE: Dialect,
        DB_HOST: string,
        DB_PASSWORD: string,
        DB_USERNAME: string,
        DB_DATABASE: string
    }
}

export const config: MysqlConfig = {
    PORT: toNumber(process.env.MYSQL_PORT) ?? 3306,
    DB: {
        DB_DATABASE: process.env.MYSQL_DB ?? '',
        DB_HOST: process.env.MYSQL_HOST ?? '127.0.0.1',
        DB_PASSWORD: process.env.MYSQL_PASSWORD ?? '123456',
        DB_TYPE: process.env.MYSQL_TYPE as Dialect ?? 'mysql',
        DB_USERNAME: process.env.MYSQL_USER ?? 'root',
    }
}

