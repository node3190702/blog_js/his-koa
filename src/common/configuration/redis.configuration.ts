import { toNumber } from "lodash";
export interface RedisConfig {
    REDIS: {
        HOST: string
        PORT: number
        USERNAME: string
        PASSWORD: string
    }
}

export const RedisConf: RedisConfig =  {
    REDIS: {
        HOST: process.env.REDIS_HOST ?? '127.0.0.1',
        PORT: process.env.REDIS_PORT ? toNumber(process.env.REDIS_PORT) : 6379,
        USERNAME: process.env.REDIS_USERNAME ?? 'default',
        PASSWORD: process.env.REDIS_PASSWORD ?? '123456',
    }

}
