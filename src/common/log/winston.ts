import winston, {format, addColors, Logform} from 'winston';

// 定义日志级别
const logLevels = {
    error: 0,
    warn: 1,
    info: 2,
    debug: 3,
};


// 配置Winston日志格式
const logFormat = format.combine(
    format.timestamp({
        format:"YY-MM-DD HH:MM:SS"
    }),
    format.printf(
        info => `${info.label}  ${info.timestamp}  ${info.level} : ${info.message}`
    ),
    format.errors({ stack: true }),
    format.prettyPrint(),
    format.label({
        label:'[LOGGER]'
    }),
    format.colorize({
        all:true
    }),
    format.splat(),
    format.json()
);

// 配置颜色
addColors({
    info: 'bold blue', // fontStyle color
    warn: 'italic yellow',
    error: 'red',
    debug: 'green',
});

// 添加日志实例
const transportArray = []
if (process.env.NODE_ENV === 'prod') {
    // 1. 文件传输（File Transport） - 记录所有级别的日志
    const errorFile = new winston.transports.File({
        filename: 'logs/error.log',
        level: 'error',
    })
    const logsFile =  new winston.transports.File({
        filename: 'logs/combined.log',
    })
    transportArray.push(errorFile, logsFile)
}

// 2. 控制台传输（Console Transport） - 根据环境变量动态设置日志级别
const console = new winston.transports.Console({
    level: process.env.NODE_ENV === 'production' ? 'info' : 'debug',
    format: winston.format.combine(
        winston.format.colorize(),
        winston.format.simple()
    )
})
transportArray.push(console)

// 创建Winston日志实例
const logger = winston.createLogger({
    levels: logLevels,
    format: logFormat,
    transports: transportArray
});

export default logger;
