import HttpStatusCode from "../constant/http-code.constants";

export class Result extends Map {
    constructor() {
        super()
        this.set('code', HttpStatusCode.OK)
        this.set('msg', 'success')
    }

    public success<T>(Object?: T) {
        if (Object !== null && Object !== undefined) {
            typeof Object === 'string' ? this.set('msg', Object) : this.set('data', Object)
        }
        return this.toObject()
    }

    public put(key: string, value: any) {
        this.set(key, value)
        return this
    }

    public toObject() {
        return Object.fromEntries(this)
    }

    public static put(key: string, value: any) {
        const r = new Result()
        r.set(key, value)
        return r
    }

    public static success <T> ( Object? : T): Result {
        const r = new Result()
        if (Object !== null && Object !== undefined) {
            typeof Object === 'string' ? r.set('msg', Object) : r.set('data', Object)
        }
        return r.toObject()
    }
}
