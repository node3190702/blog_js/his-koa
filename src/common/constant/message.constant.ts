export enum MessageConstant {
    // 登录

    LOGIN_SUCCESS = '登录成功',
    LOGIN_FAILED = '登录失败',
    LOGIN_EXPIRED = '登录过期',
    LOGIN_ERROR = '登录异常',
    LOGIN_USERNAME_NOT_EXIST = '用户名不存在',
    LOGIN_USERE_NOT_EXIST = '用户不存在',
    LOGIN_PASSWORD_EMPTY = '密码不能为空',
    LOGIN_USERNAME_ERROR = '用户名错误',
    LOGIN_PASSWORD_ERROR = '密码错误',
    LOGIN_USERINFO_NOT_CORRECT = '用户名或密码不正确',

    // token
    tokenRequiredInvalidToken= 'token验证失败',
    NOT_TOKEN_PROVIDE = '没有提供token',
    INVALID_TOKEN = '无效的token',


    // 用户权限
    USER_PERMISSION_NOT_EXIST = '用户权限不存在',
}
