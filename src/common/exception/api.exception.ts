export class ApiException extends Error {
    private readonly status: number

    constructor(status: number, message: string) {
        super();
        this.status = status
        this.message = message
    }

    get errorCode () {
        return this.status
    }

    get errorMsg() {
        return this.message
    }
}
