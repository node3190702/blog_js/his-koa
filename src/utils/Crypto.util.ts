import crypto from 'crypto'

export default class CryptoUtil {
    public static Sha256(text: string): string {
        return crypto.createHash('sha256').update(text).digest('hex')
    }

    public static MD5(text: string): string {
        return crypto.createHash('md5').update(text).digest('hex')
    }

}



