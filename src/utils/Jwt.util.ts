import jsonwebtoken from 'jsonwebtoken'
import { jwtConfig } from "../common/configuration/jwt.configuration";
import { ApiException } from "../common/exception/api.exception";
import { MessageConstant } from "../common/constant/message.constant";
import HttpStatusCode from "../common/constant/http-code.constants";
export default class Jwt {
    private Jwt() {}

    public static getJwtToken(Payload: any, SECRET_KEY: string = jwtConfig.secret, exp = jwtConfig.expiresIn): string {
        return jsonwebtoken.sign(
            Payload,
            SECRET_KEY,
            {
                expiresIn: exp, //token有效期
                // algorithm:"HS256"  默认使用 "HS256" 算法
            })
    }

    public static TokenToUserInfo(token: string): any {
        let Info: any
        try {
            Info = jsonwebtoken.verify(token, jwtConfig.secret)
        } catch (e: any) {
            console.error(e)
            throw new ApiException(HttpStatusCode.BAD_REQUEST, e.message)
        }
        return Info
    }
}
