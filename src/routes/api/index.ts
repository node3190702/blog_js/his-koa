import Router from '@koa/router'
import UserApiRouter from "./MisApiRouter/UserApiRouter";
import RoleApiRouter from "./MisApiRouter/RoleApiRouter";
import DeptApiRouter from "./MisApiRouter/DeptApiRouter";

const router = new Router({
    prefix: '/his-api'
})

/**
 * mis模块
 */
router.use("/mis", UserApiRouter, RoleApiRouter, DeptApiRouter)


export default router.routes()
