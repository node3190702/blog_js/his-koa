import Router from "@koa/router"
import { TokenValidate } from "../../../middleware/Token.middelware";
import DeptController from "../../../application/dept/dept.controller";

const router = new Router({
    prefix: "/dept"
})


/**
 * 获取所有部门
 */
router.get("/searchAllDept", TokenValidate, DeptController.getDeptList)


export default router.routes()

