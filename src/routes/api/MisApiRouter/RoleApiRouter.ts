import Router from "@koa/router";
import RoleController from "../../../application/role/role.controller";
import { TokenValidate } from "../../../middleware/Token.middelware";

const router = new Router({
    prefix: '/role'
});



router.get('/searchAllRole', TokenValidate, RoleController.searchAllRole)


export default router.routes();
