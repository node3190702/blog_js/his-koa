import Router from '@koa/router'
import MisController from "../../../application/user/user.controller";
import {
    MisLoginValidate,
    MisSearchUserByPageFormValidate,
    MisUpdateValidate
} from "../../../middleware/validate/Mis.validate";
import { TokenValidate } from "../../../middleware/Token.middelware";


const router = new Router({
    prefix: '/user'
})

/**
 * 用户登录
 */
router.post('/login',  MisLoginValidate, MisController.login)

/**
 * 用户登录退出
 */
router.get('/logout', TokenValidate, MisController.logout)

/**
 * 修改密码
 */
router.post('/updatePassword', TokenValidate, MisUpdateValidate, MisController.updatePassword)

/**
 * 分页查询
 */
router.post('/searchByPage', TokenValidate, MisSearchUserByPageFormValidate, MisController.searchByPage)

export default router.routes()
