# 使用官方 Node.js 作为基础镜像
FROM node:20.11

# 设置工作目录
WORKDIR /usr/src/app

# 假设您的 CI/CD 或 .gitlab 命令已经生成了 dist 文件夹
# 并且它位于 Docker 构建上下文的根目录中
# 复制 dist 文件夹到工作目录
COPY dist ./dist

# 暴露应用运行的端口
EXPOSE 80

# 定义容器启动命令
CMD [ "node", "dist/index.js" ]
